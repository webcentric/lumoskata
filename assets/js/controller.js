/**
 ** GLOBAL SCOPE
 */
var strGmapApiKey = '';



/**
 ** GLOBAL HELPER FUNCTIONS
 */

var empty = function( variable ) {
    //  returns true if any of the following tests succeed (empty), otherwise false (not empty)
    //  "" (an empty string)
    //  0 (0 as an integer)
    //  0.0 (0 as a float)
    //  "0" (0 as a string)
    //  NULL
    //  FALSE

    try {
        //  interrogate as variable (test for empty variable)
        if( typeof(variable) === 'undefined' || !(variable) || variable === null || variable === false || variable == 0 || variable === '' )
            return true;

        //  interrogate as array() (test for an empty array)
        if(  (Object.prototype.toString.call(variable) === '[object Array]') &&  !(variable.length > 0) )
            return true;

        //  interrogate as object{} (test for an an empty object)
        if(  (Object.prototype.toString.call(variable) === '[object Object]') &&  !(Object.keys(variable).length > 0) )
            return true;
    }
    catch( error ) {
        return true;
    }

    return false;
}

var trim = function( string, mask ) {
    if( typeof(string) != 'string' )
        return string;

    mask = !empty(mask) ? mask : /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

    return string.replace( mask, '' );
}



var CacheModel = function( strKey, intMinutes ) {

    this.strKey     = strKey;
    this.intMinutes = intMinutes;

    this.init = function() {
        Cookies.set( this.strKey, '[]', {
            expires: new Date( new Date().getTime() + this.intMinutes * 60 * 1000 )
        });
    }

    this.destroy = function() {
        Cookies.remove(this.strKey);
    }

    this.add = function( objAddress ) {
        var arrAddress = Cookies.get( this.strKey );
        arrAddress[ objAddress.getUniqueHash() ] = objAddress;
    }

    this.remove = function( objAddress ) {

    }
}


var AddressModel = function( objForm ) {

    /*
     * AddressModel Properties
     */
    this.strLine1  = trim( jQuery("#address_line1").val()  );
    this.strLine2  = trim( jQuery("#address_line2").val()  );
    this.strLocale = trim( jQuery("#address_locale").val() );
    this.strRegion = trim( jQuery("#address_region").val() );
    this.strPostal = trim( jQuery("#address_postal").val() );
    this.strHash   = '';
    this.blnCachedHit = false;
    this.isCached  = false;
    this.strGmapUrl = null;
    this.tsGeocode = null;
    this.tsGmapUrl = null;
    this.tsCacheExpiry = null;

    /*
     * AddressModel Methods
     */
    this.isValid = function() {
        return !empty(this.strLine1) && !empty(this.strLocale) && !empty(this.strRegion) && !empty(this.strPostal);
    }
    
    this.condense = function() {

        var strBuffer = '';
        
        if( !empty(this.strLine1)  )     strBuffer = strBuffer + this.strLine1;
        if( !empty(this.strLine2)  )     strBuffer = strBuffer + ' ' + this.strLine2;
        if( !empty(this.strLocale) )     strBuffer = strBuffer + ', ' + this.strLocale;
        if( !empty(this.strRegion) )     strBuffer = strBuffer + ', ' + this.strRegion;
        if( !empty(this.strPostal) )     strBuffer = strBuffer + ' ' + this.strPostal;
        
        return strBuffer;
    }

    this.geocode = function() {

        if( !this.isValid() || this.blnCachedHit )
            return false;

        // var strGmapUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.condense + '&sensor=false';
        var self = this;

        jQuery.ajax({
            method: 'GET',
            url: 'https://maps.googleapis.com/maps/api/geocode/json',
            dataType: 'json',
            data: {
                address: this.condense(),
                sensor: false,
                key: strGmapApiKey
            }
        }).done(function( objResponse ) {
            console.log("Ajax Done");
            console.log(this);
            console.log(objResponse);
        }).success(function( objResponse ) {

            if( objResponse.status == 'OK' && !empty(objResponse.results) && objResponse.results.length > 0 ) {
                if( !empty(objResponse.results[0]) ){

                    //save timestamp of response
                    self.tsGeocode = new Date();

                    //geocode results
                    self.objGeocode = objResponse.results[0];

                    self.buildGmapUrl();

                    //cache Address object for 5 minutes
                    self.tsCacheExpiry = new Date( new Date().getTime() + 5 * 60 * 1000 );
                    if( Cookies.set( 'lumos.mapcache.' + self.getUniqueHash(), JSON.stringify(self), { expires: self.tsCacheExpiry } ) )
                        self.isCached = true;
                    else
                        self.isCached = false;

                    self.refreshView();
                }
            }
            console.log(self);


        }).error(function( objResponse ) {
            console.log("Ajax Error");
            console.log(this);
            console.log(objResponse);
        });
    }

    this.refreshView = function() {
        jQuery('#gmap').attr('src', this.strGmapUrl);
        jQuery('.reactive.card-title').html(this.objGeocode.formatted_address + ' <i class="material-icons right">info_outline</i>');
        jQuery('.card-reveal .reactive.card-title').html(this.objGeocode.formatted_address + ' <i class="material-icons right">close</i>');
        jQuery('.card-reveal .reactive.map-url').attr('href', this.strGmapUrl);
        jQuery('.card-reveal .reactive.map-url').html(this.strGmapUrl);


        jQuery('.card-reveal form.output-address label').addClass('active');
        jQuery('#output-address_line1').val( this.strLine1 );
        jQuery('#output-address_line2').val( this.strLine2 );
        jQuery('#output-address_locale').val( this.strLocale );
        jQuery('#output-address_region').val( this.strRegion );
        jQuery('#output-address_postal').val( this.strPostal );
        jQuery('#output-cached_since').val( this.tsGeocode );
        jQuery('#output-cached_till').val( this.tsCacheExpiry );
        jQuery('#output-geo_lat').val( this.objGeocode.geometry.location.lat );
        jQuery('#output-geo_lng').val( this.objGeocode.geometry.location.lng );

        jQuery('.output-map').removeClass('hidden');

        if( this.blnCachedHit )
            jQuery('.reactive.cache-results').removeClass('hidden');
        else
            jQuery('.reactive.cache-results').addClass('hidden');
    }

    this.buildGmapUrl = function() {
        this.strGmapUrl = 'https://maps.googleapis.com/maps/api/staticmap?center=' + this.objGeocode.geometry.location.lat + ',' + this.objGeocode.geometry.location.lng +'&zoom=15&size=1600x1600&maptype=roadmap&markers=color:red|' + this.objGeocode.geometry.location.lat + ',' + this.objGeocode.geometry.location.lng + '&key=' + strGmapApiKey;
        this.tsGmapUrl = new Date();
        return this.strGmapUrl;
    }

    this.getUniqueHash = function() {
        if( this.isValid() && !empty(md5) ) {
            this.strHash = md5( this.condense() );
            return this.strHash;
        }
        this.strHash = '';
        return false;
    }

    /*
     * AddressModel Construction
     */

    //extract region code in the most typesafe way possible
    if( !empty(this.strRegion) && this.strRegion.includes(' - ') ) {
        this.strRegion = this.strRegion.split(' - ')[0];
    }

    //coerce all alphanumeric strings to uppercase & strip all excess punctuation and whitespace
    if( !empty(this.strLine1)  )    this.strLine1  = this.strLine1.toUpperCase().replace(/[^A-Za-z0-9\s]/g,"").replace(/\s{2,}/g, " ");
    if( !empty(this.strLine2)  )    this.strLine2  = this.strLine2.toUpperCase().replace(/[^A-Za-z0-9\s]/g,"").replace(/\s{2,}/g, " ");
    if( !empty(this.strLocale) )    this.strLocale = this.strLocale.toUpperCase().replace(/[^A-Za-z0-9\s]/g,"").replace(/\s{2,}/g, " ");

    if( this.isValid() ) {
        this.strHash = this.getUniqueHash();
        jsonCachedHit  = Cookies.get( 'lumos.mapcache.' + this.strHash );
        if( !empty(jsonCachedHit) ) {
            objCachedHit = JSON.parse( jsonCachedHit );
            this.objGeocode = objCachedHit.objGeocode;
            this.strGmapUrl = objCachedHit.strGmapUrl;
            this.tsGeocode = new Date(objCachedHit.tsGeocode);
            this.tsGmapUrl = new Date(objCachedHit.tsGmapUrl);
            this.blnCachedHit = true;

            M.toast({html: 'CACHE HIT'})

            this.tsCacheExpiry = new Date( new Date().getTime() + 5 * 60 * 1000 );
            if( Cookies.set( 'lumos.mapcache.' + this.getUniqueHash(), JSON.stringify(this), { expires: this.tsCacheExpiry } ) )
                this.isCached = true;
            else
                this.isCached = false;

            this.refreshView();
        }
        else
            M.toast({html: 'CACHE MISS'})
    }
}

$(document).ready(function(){

    $('input.autocomplete').autocomplete({
        data: {
            "AL - Alabama": null,
            "AK - Alaska": null,
            "AS - American Samoa" : null,
            "AZ - Arizona" : null,
            "AR - Arkansas" : null,
            "CA - California" : null,
            "CO - Colorado" : null,
            "CT - Connecticut" : null,
            "DE - Delaware" : null,
            "DC - District of Columbia" : null,
            "FL - Florida" : null,
            "GA - Georgia" : null,
            "GU - Guam": null,
            "HI - Hawaii" : null,
            "ID - Idaho" : null,
            "IL - Illinois" : null,
            "IN - Indiana" : null,
            "IA - Iowa" : null,
            "KS - Kansas" : null,
            "KY - Kentucky" : null,
            "LA - Louisiana" : null,
            "ME - Maine" : null,
            "MD - Maryland" : null,
            "MA - Massachusetts" : null,
            "MI - Michigan" : null,
            "MN - Minnesota" : null,
            "MS - Mississippi" : null,
            "MO - Missouri" : null,
            "MT - Montana" : null,
            "NE - Nebraska" : null,
            "NV - Nevada" : null,
            "NH - New Hampshire" : null,
            "NJ - New Jersey" : null,
            "NM - New Mexico" : null,
            "NY - New York" : null,
            "NC - North Carolina" : null,
            "ND - North Dakota" : null,
            "OH - Ohio" : null,
            "OK - Oklahoma" : null,
            "OR - Oregon" : null,
            "PA - Pennsylvania" : null,
            "PR - Puerto Rico" : null,
            "RI - Rhode Island" : null,
            "SC - South Carolina" : null,
            "SD - South Dakota" : null,
            "TN - Tennessee" : null,
            "TX - Texas" : null,
            "UT - Utah" : null,
            "VT - Vermont" : null,
            "VI - Virgin Islands" : null,
            "VA - Virginia" : null,
            "WA - Washington" : null,
            "WV - West Virginia" : null,
            "WI - Wisconsin" : null,
            "WY - Wyoming": null,
        },
    });

    $('.inputmask').inputmask();

    jQuery('form.input-api-key').submit(function(e){
        e.preventDefault();  //block default event bubbling

        strGmapApiKey = jQuery('#gmap_api_key').val();

        console.log('API Key Entered: ' + strGmapApiKey );

        if( !empty(strGmapApiKey) && strGmapApiKey.length > 8 ) {
            jQuery(".reactive-application").removeClass('hidden');

            var objTabs = M.Tabs.init( jQuery('#tabs')[0], {} );
            objTabs.updateTabIndicator();
        }


        return false;  //always return false to block form submission
    });

    jQuery('form.input-address').submit(function(e){
        e.preventDefault();  //block default event bubbling

        console.log('Form Submitted');
        var objAddress = new AddressModel(this);

        objAddress.geocode();

        if( objAddress.blnCachedHit )
            console.log(objAddress);

        return false;  //always return false to block form submission
    });
});

