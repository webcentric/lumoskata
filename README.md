## Lumos Kata
Presented by Sandon Racowsky

## Dependencies:
This Kata is implemented as a client-side, single-page application written entirely in HTML, CSS, and Javascript.  It has only three Javascript dependencies:

- jQuery >= v1.8.0 (https://github.com/jquery/jquery)
- JavaScript MD5 (https://github.com/blueimp/JavaScript-MD5) 
- JavaScript Cookie (https://github.com/js-cookie/js-cookie/wiki)

All dependencies are either included in this repository or loaded via CDN, no additional package management or dependency injection is required.

## Installation:
To install this Kata, simply clone the repository:
```
git clone https://webcentric@bitbucket.org/webcentric/lumoskata.git
```

## Documentation
Documentation has not yet been written, but is forthcoming (coming soon).  

Fortunately, this Kata is fairly easy to understand and use.  Simply input a valid Google Maps API key when prompted, and then select from either the *__Tests__*
or *__Application__* tab.

## Demo Server
This Kata is running on a demonstration server located at [http://206.189.198.51](http://206.189.198.51)  

## Supported Browsers:
This Kata is compatible with the following browsers:

- Chrome 35+
- Firefox 31+
- Safari 9+
- Opera
- Edge
- IE 11+

## Testing
Sample unit tests are presented as proof of concept in the __*Tests*__ tab, but none have yet been functionally written or implemented (coming soon).

## Changelog
- 01/07/19 - Initial Commit
